/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tema_barcharts;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.jfree.chart.*;
import java.io.File;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Mada
 */
public class Tema_barcharts {
    public JFrame f;
    public JPanel p,p_val_axe;
    public JButton b,b_file_choser;
    public JButton b_cul_font_axe,b_cul_axe,b_cul_font_pe_barcharturi,b_cul_linii_val;
    public JButton b_bg_barchart;
    public JButton b_cul_bg;
    public JCheckBox cb_valori_axe,cb_valori_pe_barcharturi,cb_linii_axe;   
    public Color color_bg=(Color.WHITE);
    public ChartPanel barPanel;
    public JFreeChart barchart;
    public JTextField tf_val_axe;
    public JLabel lbl_val_axe;
    CategoryPlot cp ;
    ValueAxis axisr ;
    CategoryAxis axisc;
    Font font ;
        
    
   public Tema_barcharts() {
       frame_fc();
   } 
   public void frame_fc() {
       JFrame f;
       JPanel p=new JPanel(new GridLayout(1,1));
       String filename=null;
       f=new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(p); 
        f.pack();
        f.setLocationRelativeTo(null);  
        f.setVisible(true);
        f.setSize(150, 150);
        JButton b_file_choser=new JButton("Alege fisier");
        b_file_choser.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               String filename;
               JFileChooser chooser=new JFileChooser();
               chooser.showOpenDialog(null);
               File f = chooser.getSelectedFile();
               filename=f.getAbsolutePath();
               frame(filename);
               throw new UnsupportedOperationException("Not supported yet."); 
           }
       });
        p.add(b_file_choser);
       
   }

   public void frame(String filename) {
       f=new JFrame();
       p=new JPanel(new GridLayout(15,2));
       p_val_axe=new JPanel(new GridLayout(1,2));
       b= new JButton();
       f.setVisible(true);
       Toolkit tk = Toolkit.getDefaultToolkit();  
       int xSize = ((int) tk.getScreenSize().getWidth());  
       int ySize = ((int) tk.getScreenSize().getHeight());  
       f.setSize(xSize,ySize);  
       f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       //loc in care vor fi inserate datele
       DefaultCategoryDataset barchartdata=new DefaultCategoryDataset(); 
       //datele vor fi inserate in tabel
       try {
	File fXmlFile = new File(filename);
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
	doc.getDocumentElement().normalize();
        /*iau toate elementele cu tagul tara*/
	NodeList nList = doc.getElementsByTagName("tara");
         /*cat timp mai am elemente de citit din xml*/
	for (int temp = 0; temp < nList.getLength(); temp++) {
		Node nNode = nList.item(temp);
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
                        String nume_tara=new String(eElement.getAttribute("id"));
                        barchartdata.setValue(Long.parseLong(eElement.getElementsByTagName("populatie").item(0).getTextContent()),nume_tara,nume_tara);
		}
	}
    } catch (Exception e) {
	e.printStackTrace();
    }
       barchart = ChartFactory.createBarChart("Populatia din lume", "Tari", "Populatie", barchartdata);
       barchart.setBackgroundPaint(Color.WHITE);
       CategoryPlot bar=new CategoryPlot(); 
       barPanel = new ChartPanel(barchart);
       barPanel.setVisible(true);  
       /*setez butonul pentru alegere background pentru a afisa un JColorChooser atunci cand este apasat*/
       b=new JButton("Alege culoarea fundalului");
       b.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
               if(color_bg==null)
                 color_bg=(Color)barchart.getBackgroundPaint();
               barchart.setBackgroundPaint(color_bg);    
               throw new UnsupportedOperationException("Not supported yet."); 
           }
       });
       
        cp = barchart.getCategoryPlot(); 
        axisr = cp.getRangeAxis();
        axisc=cp.getDomainAxis();
        /*aleg ce este scris pe buton*/
        b_cul_font_axe=new JButton("Alege culoarea fontului de pe axe");    
        /*daca butonul este apasat selectez culoarea cu ajutorul unui JColorChooser*/
        b_cul_font_axe.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
               /*daca nu este aleasa nicio culoare cu ajutorul obiectului de tip jcolochooser atunci culoarea va ramane aceeasi*/
               if(color_bg==null)
                 color_bg=(Color)axisr.getTickLabelPaint();
              axisr.setTickLabelPaint(color_bg);   
              axisc.setTickLabelPaint(color_bg);
              axisr.setLabelPaint(color_bg);
              axisc.setLabelPaint(color_bg);
               throw new UnsupportedOperationException("Not supported yet."); 
           }
       });
        b_bg_barchart=new JButton("Alege culoare background charturi");
         cp.setBackgroundPaint(Color.WHITE);
        /*daca butonul este apasat selectez culoarea cu ajutorul unui JColorChooser*/
        b_bg_barchart.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
               /*daca nu a fost aleasa nicio culoare pentru fundalul chartului atunci fundalul chartului va ramane acelasi*/
               if(color_bg==null)
                 color_bg=(Color)cp.getBackgroundPaint();
              cp.setBackgroundPaint(color_bg);
               throw new UnsupportedOperationException("Not supported yet."); 
           }
       });
        b_cul_axe=new JButton("Alege culoare axe");    
        /*daca butonul este apasat selectez culoarea cu ajutorul unui JColorChooser*/
        b_cul_axe.addActionListener(
            new ActionListener() {                   
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
               /*daca nu s-a ales nicio culoare pentru axe atunci culoarea axelor va ramane aceeasi*/
               if(color_bg==null)
                 color_bg=(Color)axisr.getAxisLinePaint();
              axisr.setAxisLinePaint(color_bg);   
              axisc.setAxisLinePaint(color_bg);             
               throw new UnsupportedOperationException("Not supported yet."); 
           }
       });
    
      //creez checkboxul si il pun checkuit la inceput  
      cb_valori_axe = new JCheckBox("Valori de pe axe visibile ?",true);
      
      //adaug un actionlistener ca sa pot sa sterg valorile si apoi sa le arat daca este checkuit sau decheckuit
    ActionListener actionListener = new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
        boolean selected = abstractButton.getModel().isSelected();
        if(selected) {
            axisc.setTickLabelsVisible(true);
            axisr.setTickLabelsVisible(true);
        }
        else
        {
            axisc.setTickLabelsVisible(false);
            axisr.setTickLabelsVisible(false);
        }
        }  
      };
    
        cb_valori_axe.addActionListener(actionListener); 
        cb_valori_pe_barcharturi = new JCheckBox("Valori de pe fiecare chart visibile ?",true);
        /*le setez sa fie inainte visibile*/
        BarRenderer bsr = (BarRenderer) cp.getRenderer();
        bsr.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        Font itemLabelFont = bsr.getBaseItemLabelFont();
        bsr.setBaseItemLabelFont(itemLabelFont.deriveFont(new Float(8.0)));
        bsr.setBaseItemLabelsVisible(true);
        bsr.setItemLabelsVisible(true);
      //adaug un actionlistener ca sa pot sa sterg valorile si apoi sa le arat pe fiecare barchart daca este checkuit sau decheckuit
    ActionListener actionListener2 = new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
        boolean selected = abstractButton.getModel().isSelected();
        if(selected) {
            //daca este bifat checkboxul atunci valorile de pe charturi sunt vizibile 
            BarRenderer bsr = (BarRenderer) cp.getRenderer();
            bsr.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
            Font itemLabelFont = bsr.getBaseItemLabelFont();
             bsr.setBaseItemLabelFont(itemLabelFont.deriveFont(new Float(8.0)));
            bsr.setBaseItemLabelsVisible(true);
              bsr.setItemLabelsVisible(true);
        }
        else
            //daca nu este bifat atunci valorile de pe charturi nu sunt vizibile
        {
            BarRenderer bsr = (BarRenderer) cp.getRenderer();
            bsr.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
            Font itemLabelFont = bsr.getBaseItemLabelFont();
             bsr.setBaseItemLabelFont(itemLabelFont.deriveFont(new Float(8.0)));
            bsr.setBaseItemLabelsVisible(false);
              bsr.setItemLabelsVisible(false);
        }
        }
      };
    
        cb_valori_pe_barcharturi.addActionListener(actionListener2); 
        
        
        
         //creez checkboxul si il pun checkuit la inceput  
      cb_linii_axe = new JCheckBox("Axe orizontale vizibile ?",true);
      
      //adaug un actionlistener ca sa pot sa sterg liniile si apoi sa le arat daca este checkuit sau decheckuit
    ActionListener actionListener3 = new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
        boolean selected = abstractButton.getModel().isSelected();
        //daca este bifat atunci liniile orizontqale se fac vizibile
        if(selected) {
            cp.setRangeGridlinesVisible(true);
        }
        else
         //altfel sunt facute invizibile
        {
            cp.setRangeGridlinesVisible(false);
        }
        }
      };
    
        cb_linii_axe.addActionListener(actionListener3); 
        /*aleg ce este scris pe buton*/
        b_cul_font_pe_barcharturi=new JButton("Alege culoarea valorilor punctelor");        
        /*daca butonul este apasat selectez culoarea cu ajutorul unui JColorChooser*/
         b_cul_font_pe_barcharturi.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
                BarRenderer bsr = (BarRenderer) cp.getRenderer();
               bsr.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
             Font itemLabelFont = bsr.getBaseItemLabelFont();
               if(color_bg==null)
               {
                   /*daca nu este aleasa nicio culoare atunci culoarea valorilor punctelor va ramane aceeasi*/
                   color_bg=(Color)bsr.getBaseItemLabelPaint();
               }
             bsr.setBaseItemLabelFont(itemLabelFont.deriveFont(new Float(8.0)));
             bsr.setBaseItemLabelPaint(color_bg);
             
             
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
       });       
         //setez liniile corespunzatoare valorilor de pe axe cu negru
         cp.setRangeGridlinePaint(Color.BLACK);        
        //setare buton pentru culoare linii coresp val de pe axe
        b_cul_linii_val=new JButton("Alege culoare linii coresp. val. pe axe"); 
        /*atunci cand butonul este apasat sa apara JColoChooser-ul*/
        b_cul_linii_val.addActionListener(
            new ActionListener() {
                    
           @Override
           public void actionPerformed(ActionEvent ae) {
               color_bg=JColorChooser.showDialog(null,"Pick Color",color_bg);
               /*culoarea liniilor va ramane aceeasi daca nu este aleasa nicio culoare*/
               if(color_bg==null) {
                 color_bg=(Color)cp.getRangeGridlinePaint();
               }
              cp.setRangeGridlinePaint(color_bg);
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
       });
        tf_val_axe=new JTextField(5);   
        /*setez incrementarea valorilor de pe axa verticala cu 200k , adica va fi de genul 0 200k 400k etc*/
        tf_val_axe.setText("20000000");
        NumberAxis na=(NumberAxis)axisr;
        /*Setez valoarea de pe axe din 200 in 200*/
        na.setTickUnit(new NumberTickUnit(20000000));
        tf_val_axe.addActionListener(new ActionListener(){

                public void actionPerformed(ActionEvent e){
                    String aux=tf_val_axe.getText();
                    try  
                    {  
                      double d = Double.parseDouble(aux);  
                      NumberAxis na=(NumberAxis)axisr;
                      na.setTickUnit(new NumberTickUnit(d));
                     }  
                     catch(NumberFormatException nfe)  
                    {  
                    }    
                    }});
       lbl_val_axe=new JLabel("Unitatea pe axa este : "); 
      // cp = barchart.getCategoryPlot();   
       /*in randurile urmatoare adaug toate obiectele la panelul meu*/
       f.add(barPanel,BorderLayout.CENTER);
       p.add(b);
       p.add(b_cul_font_axe);
       p.add(b_cul_axe);
       p.add(b_cul_font_pe_barcharturi);
       p.add(b_cul_linii_val);
       p.add(b_bg_barchart);
       p_val_axe.add(lbl_val_axe);
       p_val_axe.add(tf_val_axe);
       p.add(p_val_axe);
       p.add(cb_valori_axe);
       p.add(cb_valori_pe_barcharturi);
       p.add(cb_linii_axe);
       f.add(p,BorderLayout.EAST);
   }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //creez un obiect de tipul Tema_barcharts
         new Tema_barcharts();
  }
        
    }
    

